console.log('hello');

// you can write to stdout for debugging purposes, e.g.
// console.log('this is a debug message');

function solution(N, S, T) {
    // write your code in JavaScript (Node.js 6.4.0)
    let ships = parseShips(S, N * N);
    let hits = parseHits(T, N * N);

    let touched = 0,
        sunken = 0;

    for (let ship of ships) {
        let touching = calcHits(ship, hits);
        if (touching > 0) {
            if (touching == getSize(ship)) {
                sunken++;
            } else {
                touched++;
            }
        }
    }

    return "" + sunken + "," + touched;
}

function getSize(ship) {
    return (Math.abs(ship.topLeft.x - ship.bottomRight.x) + 1) *
        (Math.abs(ship.topLeft.y - ship.bottomRight.y) + 1);
}

function calcHits(ship, hitsList) {
    let hits = 0;
    for (let shot of hitsList) {
        if (isGreaterOrEqual(shot, ship.topLeft) &&
            isGreaterOrEqual(ship.bottomRight, shot)) {
            hits++;
        }
    }
    return hits;
}

function isGreaterOrEqual(shotPoint, shipPoint) {
    return shotPoint.x >= shipPoint.x && shotPoint.y >= shipPoint.y;
}

function parseHits(hits, maxHits) {
    let hitsList = [];
    let coords = hits.split(" ");
    for (let coord of coords) {
        console.log(coord);

        let point = {
            x: coord[coord.length - 1],
            y: coord.substr(0, coord.length - 1)
        };

        console.log(point);
        hitsList.push(point);
    }
    return hitsList;
}

function parseShips(ships, maxShips) {
    let shipsList = [];
    let shipsCoords = ships.split(",");
    for (let shipCoord of shipsCoords) {
        console.log(shipCoord);
        let coords = shipCoord.split(" ");
        let ship = {
            topLeft: {
                x: coords[0][coords[0].length - 1],
                y: coords[0].substr(0, coords[0].length - 1)
            },
            bottomRight: {
                x: coords[1][coords[1].length - 1],
                y: coords[1].substr(0, coords[1].length - 1)
            }
        }
        console.log(ship);
        shipsList.push(ship);
    }

    return shipsList;
}

// function solution(A) {
//     console.log('hello 1');
//     var ones = 0,
//         passing = 0;
//     for (var i = A.length - 1; i >= 0; i--) {
//         if (A[i] === 0) {
//             passing += ones;
//             if (passing > 1000000000) {
//                 return -1;
//             }
//         } else {
//             ones++;
//         }
//     }
//     return passing;
// }

// function solution(A) {
//     let n = A.length;

//     if (n < 3) return 0; //mininum length of arithmetic slice is 3

//     let start = 0; //start of a slice

//     let numOfSlices = 0; //answer we want

//     while (start < n - 2) {
//         let end = start + 1; //end of a slice
//         let diff = A[end] - A[start]; //diff of the arithmetic

//         //extend until arithmetic condition fails
//         while (end < n - 1 && A[end + 1] - A[end] == diff) {
//             end++;
//         }

//         let lenOfSlice = end - start + 1; //length of the found arightmetic slice

//         //valid slice must have at least 3 number
//         if (lenOfSlice >= 3) {

//             //For an arithmetic progression with length n, where n>3, the number of slices it can form is
//             // (n-2)*(n-1)/2. For example, if n = 5, it has 3 slices with length 3, 2 slices with length 4, 
//             // and 1 slice with length 5.
//             numOfSlices += ((lenOfSlice - 2) * (lenOfSlice - 1) / 2);
//         }

//         //Arithmetic progression will not overlap, so move start to end
//         start = end;
//     }

//     //consider overflow
//     return (numOfSlices > 1000000000) ? -1 : numOfSlices;
// }

// let array1 = [0, 1, 0, 1, 1];
let array1 = [-1, 1, 3, 3, 3, 2, 1, 0];

console.log(solution(array1));