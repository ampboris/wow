// you can write to stdout for debugging purposes, e.g.
// console.log('this is a debug message');

function solution(A) {
    let n = A.length;

    if (n < 3) return 0;

    let start = 0;
    let numOfSlices = 0;

    while (start < n - 2) {
        //end of a slice
        let end = start + 1;
        //diff of the arithmetic
        let diff = A[end] - A[start];

        //extend until arithmetic condition fails
        while (end < n - 1 && A[end + 1] - A[end] == diff) {
            end++;
        }
        //diff of the arithmetic
        let lenOfSlice = end - start + 1;

        //valid slice must have at least 3 number
        if (lenOfSlice >= 3) {
            numOfSlices += ((lenOfSlice - 2) * (lenOfSlice - 1) / 2);
        }

        //reset to new start
        start = end;
    }

    //overflow condition
    return (numOfSlices > 1000000000) ? -1 : numOfSlices;
}

// Running solution...
// Compilation successful.

// Example test: [-1, 1, 3, 3, 3, 2, 1, 0]
// OK

// Your code is syntactically correct and works properly on the example test.
// Note that the example tests are not part of your score.On submission at least 8 test cases not shown here will assess your solution.