// you can write to stdout for debugging purposes, e.g.
// console.log('this is a debug message');

function solution(N, S, T) {
    // write your code in JavaScript (Node.js 6.4.0)
    let ships = parseShips(S, N * N);
    let hits = parseHits(T, N * N);

    let touched = 0,
        sunken = 0;

    for (let ship of ships) {
        let touching = calcHits(ship, hits);
        // console.log(ship, touching);
        if (touching > 0) {
            if (touching == getSize(ship)) {
                sunken++;
            } else {
                touched++;
            }
        }
    }

    return "" + sunken + "," + touched;
}

function parseHits(hits, maxHits) {
    let hitsList = [];
    let coords = hits.split(" ");
    for (let coord of coords) {
        // console.log(coord);

        let point = {
            x: charToInt(coord[coord.length - 1]),
            y: coord.substr(0, coord.length - 1) - 1
        };

        // console.log(point);
        hitsList.push(point);
    }
    return hitsList;
}

function charToInt(input) {
    // convert upper chase character to int
    return input.toUpperCase().charCodeAt(0) - 65;
}

function parseShips(ships, maxShips) {
    let shipsList = [];
    let shipsCoords = ships.split(",");
    for (let shipCoord of shipsCoords) {
        // console.log(shipCoord);
        let coords = shipCoord.trim().split(" ");
        let ship = {
                topLeft: {
                    x: charToInt(coords[0][coords[0].length - 1]),
                    y: coords[0].substr(0, coords[0].length - 1) - 1
                },
                bottomRight: {
                    x: charToInt(coords[1][coords[1].length - 1]),
                    y: coords[1].substr(0, coords[1].length - 1) - 1
                }
            }
            // console.log(ship);
        shipsList.push(ship);
    }

    return shipsList;
}

function getSize(ship) {
    return (Math.abs(ship.topLeft.x - ship.bottomRight.x) + 1) *
        (Math.abs(ship.topLeft.y - ship.bottomRight.y) + 1);
}

function calcHits(ship, hitsList) {
    let hits = 0;
    for (let shot of hitsList) {
        if (isGreaterOrEqual(shot, ship.topLeft) &&
            isGreaterOrEqual(ship.bottomRight, shot)) {
            hits++;
        }
    }
    return hits;
}

function isGreaterOrEqual(shotPoint, shipPoint) {
    return shotPoint.x >= shipPoint.x && shotPoint.y >= shipPoint.y;
}

// Your test
// case :[10, '1A 2B, 5D 7D', '3A 7C 2E 4D 8C']
// Returned value:
//     '0,0'

//     Your test
// case :[10, '1A 2B,5D 7D', '1A 2A 2B 4D 1B']
// Returned value:
//     '1,0'

//     Example test: (4, '1B 2C,2D 4D', '2B 2D 3D 4D 4A')
//     OK

//     Example test: (3, '1A 1B,2C 2C', '1B')
//     OK

//     Example test: (12, '1A 2A,12A 12A', '12A')
//     OK